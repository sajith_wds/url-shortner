<?php

class Url_model extends CI_Model
{


    function __construct()
    {
// Call the Model constructor
        parent::__construct();
    }

    public function isValidToken($token = '')
    {
        $response = true;//default response
        if ($token != '') {
            $this->db->where('slug', trim($token));
            $result = $this->db->get('us_urls')->result_array();
            if (count($result) > 0) {//token exists
                $response = false;
            }
        }
        return $response;
    }

    public function addNewURL($data)
    {
        $this->db->insert('us_urls', $data);
    }

    public function isExistingURL($url = '')
    {
        $response = false;// Default response
        if ($url != '') {
            $this->db->where('url', trim($url));
            $result = $this->db->get('us_urls')->result_array();
            if (count($result) > 0) {//url exists
                $response = $result[0];
            }
        }
        return $response;
    }

    public function getURLBySlug($slug = '')
    {
        $response = false;// Default response
        if ($slug != '') {
            $this->db->where('slug', trim($slug));
            $result = $this->db->get('us_urls')->result_array();
            if (count($result) > 0) {//url exists
                $response = $result[0];
            }
        }
        return $response;
    }
}
