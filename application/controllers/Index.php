<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('url_model'));//Loading the required models
        $this->load->helper(array('string'));//Loading the required helpers
    }

    public function index()
    {
        $this->load->view('index');//rendering default view
    }

    public function processurl()
    {
        $response = array('status' => 'failed', 'message' => "Invalid request.", 'url' => '');//Default response
        if ($this->input->is_ajax_request()) {// check is a valid ajax request
            if (isset($_POST['url_to_shorten']) && trim($_POST['url_to_shorten']) != '') {// Checking isvalid request
                $url = trim($_POST['url_to_shorten']);//POST data to variable by trimming
                //URL Exists checking
                $is_exists = $this->url_model->isExistingURL($url);
                if (!$is_exists) {

                    //Token generation using random string
//                    $valid_token = false;//default validity of token
//                    while ($valid_token == false) {// generating a unused token for the URl
//                        $token = random_string('alnum', 16);
//                        $valid_token = $this->url_model->isValidToken($token);
//                    }
                    //Token generation using random string ^


                    $token = uniqid(random_string('alnum', 3));// token generation using current timestamp and random
                    $data_array = array(
                        'url' => $url,
                        'slug' => $token,
                        'created_time' => date('Y-m-d h:i:s')
                    );//array for insertion
                    $shortened_url = site_url() . 'redirect/' . $token;//shortened URL
                    $this->url_model->addNewURL($data_array);//adding URL to DB
                    $response = array('status' => 'success', 'url' => $shortened_url);//success response
                } else {
                    $shortened_url = site_url() . 'redirect/' . $is_exists['slug'];//shortened URL
                    $response = array('status' => 'failed', 'message' => "This URL already exists.", 'url' => $shortened_url);//URl Exists response
                }
            }
        }
        die(json_encode($response));//generating json response
    }

    public function redirect($slug = '')
    {
        $url = $this->url_model->getURLBySlug($slug);//get URL to redirect by slug
        if ($url) {// URL exists
            redirect($url['url']);//redirect to actual URL
        } else {//URL does not exists
            echo 'Invalid request.';// Invalid request
        }
        exit();
    }
}
