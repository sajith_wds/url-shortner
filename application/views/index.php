<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= APP_TITLE ?></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"/>
    <!-- custom styling -->
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>"/>
    <!-- sweet alert -->
    <link rel="stylesheet" href="<?= base_url('assets/css/sweetalert.css') ?>"/>
</head>
<body>
<div class="row wrapper">
    <div class="col-md-6 col-md-offset-3">
        <form class="form-inline" method="post" name="shorten-url-frm" id="shorten-url-frm">
            <div class="form-group">
                <div class="input-group">
                    <input type="url" name="url_to_shorten" class="form-control" id="url" placeholder="URL to Shorten">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Shorten</button>
        </form>
    </div>
    <div class="col-md-6 col-md-offset-3 url-container">
        <div class="loading">
            <div class="windows8">
                <div class="wBall" id="wBall_1">
                    <div class="wInnerBall"></div>
                </div>
                <div class="wBall" id="wBall_2">
                    <div class="wInnerBall"></div>
                </div>
                <div class="wBall" id="wBall_3">
                    <div class="wInnerBall"></div>
                </div>
                <div class="wBall" id="wBall_4">
                    <div class="wInnerBall"></div>
                </div>
                <div class="wBall" id="wBall_5">
                    <div class="wInnerBall"></div>
                </div>
            </div>
        </div>
        <h5 class="shortened_url"></h5>
    </div>
</div>
<!-- Script config vars -->
<script>
    var site_url = '<?= site_url() ?>';
</script>
<!-- Jquery from CDN -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<!-- Sweet alert -->
<script src="<?= base_url('assets/js/sweetalert.min.js') ?>"></script>
<!-- Jquery Validate -->
<script src="<?= base_url('assets/js/jquery.validate.min.js') ?>"></script>
<!-- Custom scripts -->
<script src="<?= base_url('assets/js/main.js') ?>"></script>
</body>
</html>