$(document).ready(function () {
    //validating URl accepting form
    $("#shorten-url-frm").validate({
        rules: {
            url_to_shorten: {
                required: true,
                url: true
            }
        },
        messages: {
            url_to_shorten: {
                required: "Please enter a URL.",
                url: "Please enter a valid URL."
            }
        },
        submitHandler: function (form, e) {
            e.preventDefault();
            $('.loading').show();
            var form_data = $('#shorten-url-frm').serialize();

            $.post(site_url + '/index.php/index/processurl/', form_data, function (response) {
                var data = $.parseJSON(response);
                if (data.status == 'success') {
                    $('.shortened_url').html('<a target="_blank" href="'+data.url+'" title="Go To URL">'+data.url+'</a>');
                    $('.loading').hide();
                } else {
                    $('.shortened_url').html('<a target="_blank" href="'+data.url+'" title="Go To URL">'+data.url+'</a>');
                    swal("Error", data.message, "error");
                    $('.loading').hide();
                }
            });
        },
        errorPlacement: function (error, element) {
            error.insertBefore(element.parent('.input-group').parent('.form-group'));
        }
    });

});